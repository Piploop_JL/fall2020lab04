//Jimmy Le, 1936415
package geometry;

public class Rectangle implements Shape{

	
	private double length;
	private double width;
	
	public Rectangle(double newLength, double newWidth) {
		length = newLength;
		width = newWidth;
	}
	
	public double getLength() {
		return length;
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getArea() {
		double area = length*width;
		return area;
	}
	
	public double getPerimeter() {
		double perimeter = 2*(length+width);
		return perimeter;
	}
}
