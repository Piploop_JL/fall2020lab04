//Jimmy Le, 1936415
package geometry;

public interface Shape {
	double getArea();
	double getPerimeter();

}
