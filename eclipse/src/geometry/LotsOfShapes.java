//Jimmy Le 1936415
package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		
		Shape[] shapeArr = new Shape[5];
		//shapeArr[0] = new Shape();
		shapeArr[0] = new Rectangle(3,4);
		shapeArr[1] = new Rectangle(2,5);
		shapeArr[2] = new Circle(4);
		shapeArr[3] = new Circle(2);
		shapeArr[4] = new Square(3);
		
		
		for (int i = 0; i < shapeArr.length ; i++) {
			System.out.println(shapeArr[i].getClass());
			System.out.println("Area : " + shapeArr[i].getArea());
			System.out.println("Perimeter : " + shapeArr[i].getPerimeter());
			System.out.println();
		}
	}
}
