//Jimmy Le, 1936415
package geometry;

public class Circle implements Shape{

		private double radius;
		
		public Circle(double newRadius) {
			radius = newRadius;
		}
		
		public double getRadius() {
			return radius;
		}
		
		public double getArea() {
			double area = Math.PI*(radius*radius);
			return area;
		}
		
		public double getPerimeter() {
			double perimeter = Math.PI*(2*radius);
			return perimeter;
		}
}
