//Jimmy Le, 1936415
package inheritance;

public class ElectronicBook extends Book {

	private int numberByte;
	
	public ElectronicBook(String newTitle, String newAuthor, int newByte) {
		
		super(newTitle, newAuthor);
		//Book test = new Book(newTitle, newAuthor);
		numberByte = newByte;
		
	}
	
	public String toString() {
		String fromBase = super.toString();
		return(fromBase + " Number of Byte : "+ numberByte);
	}
	
	
	
	
	
	
	
	
	
}
