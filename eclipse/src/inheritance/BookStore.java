//Jimmy Le, 1936415
package inheritance;

public class BookStore {

	
	public static void main(String[] args) {
		Book[] bookArr = new Book[5];
		bookArr[0] = new Book("book1", "author1");
		bookArr[1] = new ElectronicBook("book2", "author2", 2);
		bookArr[2] = new Book("book3", "author3");
		bookArr[3] = new ElectronicBook("book4", "author4", 4);
		bookArr[4] = new ElectronicBook("book5", "author5", 5);
		
		for (int i = 0; i < bookArr.length ; i++) {
			System.out.println(bookArr[i]);
		}
	}
}
