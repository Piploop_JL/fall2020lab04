//Jimmy Le, 1936415
package inheritance;

public class Book {
	
	protected String title;
	private String author;
	
	
	//getters
	public String getTitle() {
		return title;
	}
	
	public String getAuthor() {
		return author;
	}
	
	//constructor
	public Book(String newTitle, String newAuthor) {
		title = newTitle;
		author = newAuthor;
	}
	//Overriding the toString()
	public String toString() {
		return("Title : " + title + " Author : " + author);
	}

}
